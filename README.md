"Mountains locator" project

About:

This simple application displays the nearest mountains and hills located 
within a radius of 70 kilometers from user's current location. It detects the location
with help of ip-api and geonames Api.It also has login (email) and password entry.You can set any combination of characters allowed by HTML
email type validation - here:between 3 and 20 chars in email input. Range for password input is 3 to 8 chars.



Details:
- used Jquery
- used Api (http://ip-api.com/json)
- used Api (http://www.geonames.org/export/web-services.html) 
- used Bootstrap 
